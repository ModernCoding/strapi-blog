import type { Schema, Attribute } from '@strapi/strapi';

export interface BlogFeaturedPost extends Schema.Component {
  collectionName: 'components_blog_featured_posts';
  info: {
    displayName: 'FeaturedPost';
    icon: 'filter';
  };
  attributes: {
    FeaturedPostHeading: Attribute.String;
    Post: Attribute.Relation<
      'blog.featured-post',
      'oneToOne',
      'api::post.post'
    >;
  };
}

export interface BlogPostSelection extends Schema.Component {
  collectionName: 'components_blog_post_selections';
  info: {
    displayName: 'PostSelection';
    icon: 'layer';
  };
  attributes: {
    Tags: Attribute.Relation<
      'blog.post-selection',
      'oneToMany',
      'api::tag.tag'
    >;
    SelectionHeading: Attribute.String;
  };
}

export interface LayoutButton extends Schema.Component {
  collectionName: 'components_layout_buttons';
  info: {
    displayName: 'Button';
    icon: 'grid';
  };
  attributes: {
    Label: Attribute.String & Attribute.Required;
    Link: Attribute.String & Attribute.Required;
  };
}

export interface LayoutHeroSection extends Schema.Component {
  collectionName: 'components_layout_hero_sections';
  info: {
    displayName: 'HeroSection';
    icon: 'layout';
  };
  attributes: {};
}

export interface SeoSeoInformation extends Schema.Component {
  collectionName: 'components_seo_seo_informations';
  info: {
    displayName: 'SeoInformation';
    icon: 'feather';
  };
  attributes: {
    SeoTitle: Attribute.String;
    SeoDescription: Attribute.String;
  };
}

declare module '@strapi/types' {
  export module Shared {
    export interface Components {
      'blog.featured-post': BlogFeaturedPost;
      'blog.post-selection': BlogPostSelection;
      'layout.button': LayoutButton;
      'layout.hero-section': LayoutHeroSection;
      'seo.seo-information': SeoSeoInformation;
    }
  }
}
